using Godot;
using System;

public class Slash : Area2D
{
	int Team = 0;
	int Damage = 1;
	bool enabled = true;
	AnimationPlayer AnimationPlayer;
	public static Slash GetInstance(int team, int damage)
	{
		Slash slash = (Slash)ResourceLoader.Load<PackedScene>("res://Scenes/Slash.tscn").Instance();
		slash.Team = team;
		slash.Damage = damage;
		return slash;
	}

	public static CPUParticles2D GetSlashHitInstance()
	{
		return (CPUParticles2D)ResourceLoader.Load<PackedScene>("res://Scenes/Fx/SlashHit.tscn").Instance();
	}

	public override void _Ready()
	{
		AnimationPlayer = GetNode<AnimationPlayer>("AnimationPlayer");
	}

	private void DisableSlash()
	{
		enabled = false;
	}

	private void _on_Slash_body_entered(object body)
	{
		if(body is Humanoid && ((Humanoid)body).Team != Team && enabled)
		{
			Humanoid humanoid = ((Humanoid)body);
			Vector2 impulseDir = GlobalPosition.DirectionTo(humanoid.Position);
			humanoid.GetHit(Damage, impulseDir * 200);	
			// CPUParticles2D hitParticles = GetSlashHitInstance();
			// hitParticles.GlobalPosition = humanoid.Position;
			// GetNode("/root/Game/FxContainer").AddChild(hitParticles);
			// hitParticles.Emitting = true;
		}
		
	}
}



