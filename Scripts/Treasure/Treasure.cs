using Godot;
using System;

public class Treasure : Sprite
{
	public Humanoid CarriedBy = null;
	public Humanoid BeingTakenBy = null;
	private Vector2 originalPosition;
	public override void _Ready()
	{
		originalPosition = Position;
	}

	public override void _Process(float delta)
	{
		if (null != CarriedBy)
		{
			Position = CarriedBy.Position + new Vector2(0, -30);
		}
	}

	public void Reset()
	{
		CarriedBy = null;
		BeingTakenBy = null;
		Position = originalPosition;
	}
}
