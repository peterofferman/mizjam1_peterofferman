using Godot;
using System;

public class Player : Humanoid
{
	private int _speedLevel = 1;
	public int SpeedLevel
	{
		get { return _speedLevel; }
		set
		{
			_speedLevel = value;
			walkSpeed = 40 + 30 * _speedLevel;
		}
	}
	public int SwordLevel = 1;
	public int BowLevel = 1;

	int selectedWeapon = 1;
	Sprite BowCrosshair;
	Sprite Crosshair;
	Camera2D camera;
	public override void _Ready()
	{
		SpeedLevel = 1;
		Crosshair = GetNode<Sprite>("Crosshair");
		BowCrosshair = Crosshair.GetNode<Sprite>("BowCrosshair");
		camera = GetNode<Camera2D>("Camera2D");
		base._Ready();
	}

	public override void _Process(float delta)
	{
		Vector2 localMousePos = GetLocalMousePosition();
		Crosshair.Position = localMousePos;
		if (selectedWeapon == 2)
		{
			float radius = localMousePos.Length() * accuracyFactor;
			float scale = radius / 7;
			BowCrosshair.Scale = new Vector2(scale, scale);
			BowCrosshair.Rotation += delta;
		}

		direction = localMousePos.Normalized();
		Blocking = Input.IsActionPressed("block") && SwordTimer.TimeLeft <= 0;

		base._Process(delta);

		if (Input.IsActionJustPressed("select_weapon_1"))
		{
			SelectWeapon(1);
		}
		else if (Input.IsActionJustPressed("select_weapon_2"))
		{
			SelectWeapon(2);
		}
		else if (Input.IsActionJustPressed("select_weapon_3"))
		{
			SelectWeapon(3);
		}

		if (Input.IsActionJustPressed("attack"))
		{
			switch (selectedWeapon)
			{
				case 1:
					SlashSword(SwordLevel);
					break;
				case 2:
					FireArrows(GetGlobalMousePosition(), BowLevel);
					break;

			}

		}
		camera.Position = localMousePos * 0.1f;

	}

	public override void _PhysicsProcess(float delta)
	{
		walkDir.x = Input.GetActionStrength("right") - Input.GetActionStrength("left");
		walkDir.y = Input.GetActionStrength("down") - Input.GetActionStrength("up");
		base._PhysicsProcess(delta);
	}

	private void SelectWeapon(int weaponId)
	{
		selectedWeapon = weaponId;
		switch (weaponId)
		{
			case 1:
				Sword.Visible = true;
				Shield.Visible = true;
				Bow.Visible = false;
				BowCrosshair.Visible = false;
				break;
			case 2:
				Sword.Visible = false;
				Shield.Visible = false;
				Bow.Visible = true;
				BowCrosshair.Visible = true;
				break;
		}
	}
}
